"use strict";
(function(window){

	var Soul = window.Soul,
		elements = Soul.elements ,
		base = elements.base ;
	/**
	 * Apply alpha to an element
	 * 
	 * @param a float 0.0 - 1.0
	 * @returns
	 */
	base.prototype.alpha = function(a) {
		
		this.prep = this.prep || {};
		
		if (typeof a == 'undefined' ) return this.prep['globalAlpha'];
		else if (a > 1 ) a = a / 100;
		
		if(!a && typeof this.prep['globalAlpha'] != 'undefined') {
			delete this.prep['globalAlpha'];
		} else {
			this.prep['globalAlpha'] = a;
		}
		return this;
	};
	
	/**
	 * {Internal Function}
	 * 
	 * @param k
	 * @param v
	 * @returns
	 */
	base.prototype.inherit = function(k, v)
	{
		if ( typeof this.__inherit__ == 'undefined' )
			this.__inherit__ = {};
		
		if( typeof v != 'undefined' )
			this.__inherit__ [k] = v;
		
		return this.__inherit__ [k];
	};
	
	var translate = function(o, how)
	{
		if(how && how.x && how.y)
			return [ o.x + how.x, o.y + how.y ];
		
		switch(how) {
			case 'top-left':
				return [ o.x, o.y ]
				break;
			case 'top-center':
				return [ ( o.x + o.width / 2), o.y ]
				break;
			case 'top-right':
				return [ o.x + o.width, o.y ]
				break;
			case 'center-left':
				return [ o.x, (o.y + o.height / 2) ]
				break;
			case 'center-right':
				return [ o.x + o.width, (o.y + o.height / 2) ]
				break;
			case 'bottom-left':
				return [ o.x , o.y + o.height ]
				break;
			case 'bottom-center':
				return [ ( o.x + o.width / 2), o.y + o.height ]
				break;
			case 'bottom-right':
				return [ o.x + o.width, o.y + o.height ]
				break;
			case 'center':
			default:
				return [ ( o.x + o.width / 2), (o.y + o.height / 2) ]
				break;
		}
	}
	
	/**
	 * rotate an element on its center
	 * 
	 * @param angle
	 * @param origin top-left, top-center, top-right, center-left, center, center-right, bottom-left, bottom-center, bottom-right, { x, y }
	 * @returns
	 */
	base.prototype.rotate = function(angle,origin) {
		this.prep = this.prep || {};
		this.transform = this.transform || {};
		
		if(angle === false ) {
			delete this.prep['rotate'];
			delete this.prep['translate'];
			this.transform.x = 0;
			this.transform.y = 0;
			this.inherit('rotate', false);
			return true;
		}
		
		var d = this.getDimensions();
		
		if( angle === true && this.position == 'relative') {
			var p = this.parent(), pd = p.getDimensions();
			this.prep['translate'] = translate(pd,origin) ;
			this.transform.x = pd.transform.x ;
			this.transform.y = pd.transform.y ;
			this.prep['rotate'] = p.prep['rotate']  ;
			this.inherit('rotate', true);
			return true;
		}
		
		if (typeof angle == 'undefined' ) return this.prep['rotate'];
		else if (angle > 360 || angle < 0 ) angle = angle % 360 ;
		
		if ( typeof this.x  != 'undefined' && typeof this.y  != 'undefined'  ) {
			this.prep['translate'] = translate(d,origin) ;
			this.transform.x = -(this.prep['translate'][0]) ;
			this.transform.y = -(this.prep['translate'][1]) ;
		}

		this.prep['rotate'] = angle * Math.PI / 180  ;
		for(var child in this.__children__) {
			child = this.__children__[child];
			if( child.inherit('rotate') == true || typeof child.inherit('rotate') == 'undefined' )
				child.rotate(true,origin);
		}
		
		return this;
	};
	
	
	
	/**
	 * Zoom an element, if applicable
	 * 
	 * @param multiplier
	 */
	base.prototype.zoom = function(multiplier)
	{
		if(!this.size || !this.size.width || !this.size.height)
			throw Soul.Exception(this,this,'zoom effect not supported');
			
		this.width = this.size.width * multiplier;
		this.height = this.size.height * multiplier;

		return true;
	};
	
	Soul.loaded('effects');
	
})(window);