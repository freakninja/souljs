"use strict";
(function(window){
	
	var document = window.document,
		rootSoul = window.Soul,
		soul, Soul;
	
	Soul = function(element) {
		
		if(typeof element == 'undefined') return;
		
		soul = soul || new Soul;
		
		if(element.type && /^soul\//.test(element.type)) return element;
		
		if(/^\#/.test(element) ) {
			element = soul.byId(element);
			if(element && !element.__soul__) soul.createElement(element);
		} else if ( /^\./.test(element) ) {
			throw Soul.Exception(element,this,'Not supported yet');
		} else if ( /^\</.test(element) ) {
			element = soul.parseElement(element);
			soul.createElement(element);
		} else if ( typeof element.type != 'undefined' ) {
			soul.createXElement(element);
		} else if ( typeof element.__soul__  == 'undefined' ) {
			soul.createElement(element);
		}
		
		return element.__soul__;
	};
	
	Soul.prototype.byClass = ( document.getElementsByClassName ) ? function(cn) { return document.getElementsByClassName(cn.substr(1)); } :  function(className) {
		className = className.substr(1);
		var retnode = [];
		var elem = this.getElementsByTagName('*');
		for (var i = 0; i < elem.length; i++) {
			if ((' ' + elem[i].className + ' ').indexOf(' ' + className + ' ') > -1)
				retnode.push(elem[i]);
		}
		return retnode;
	};
	
	Soul.prototype.byId = function(id) { return document.getElementById(id.substr(1)) };
		
	Soul.prototype.parseElement = function(element)
	{
		var c = document.createElement('div');
		c.innerHTML = element;
		element = c.childNodes[0];
		return element;
	};
		
	Soul.prototype.createElement = function(element)
	{
		var elm = element.tagName.toLowerCase();
		if(typeof Soul.elements[elm] != 'undefined' ) {
			element.__soul__ = new Soul.elements[elm](element);
			return element.__soul__;
		}
		throw Soul.Exception(element,this,"Element '"+element.tagName+"' not supported");
	};
	
	Soul.prototype.createXElement = function(element)
	{
		if(typeof Soul.elements[element.type] != 'undefined' ) {
			element.__soul__ = new Soul.elements[element.xType](element);
			return element.__soul__;
		}
		throw Soul.Exception(element,this,"Element '"+element.type+"' not supported");
	};
	
	for(var p in rootSoul ) {
		Soul[p] = rootSoul[p];
	};
	
	window.Soul = Soul;
	
	Soul.loaded('engine');
	
	Soul.require('canvas');
	
})(window);