"use strict";
(function(window){

	var Soul = window.Soul,
		elements = Soul.elements ,
		base = elements.base ;

	Soul.dispatchEvent = function(eventType, eventArgs, source)
	{
		var event; // The custom event that will be created
		event = new Event(eventType);
		event.source    = source;
		event.arguments = eventArgs;
		if (document.createEvent) {
			document.dispatchEvent(event);
		} else {
			document.fireEvent("on" + event.eventType, event);
		}
	}
	
	base.prototype.dispatchEvent = function(eventType, eventArgs)
	{
		Soul.dispatchEvent(eventType, eventArgs, this);
	};
	
	base.prototype.on = function(event, fn)
	{
		if(!event)
			return;
		
		if(!this.eventListners)
			this.eventListners = {};
		
		var self = this;
		
		self.eventListners[event] = fn;
		
		var wrap = function(e) {
			console.log(e.type , e.source.type, e.source.guid());
			if(self.guid() == e.source.guid())
				self.eventListners[event].call(self,e);
		};
		
		if(event && !event.push && !event.pop) {
			document.addEventListener(event, wrap , false);
			return this;
		}
		
		for(var e in event) {
			e = event[e];
			document.addEventListener(e , wrap , false);
		}
		
		return this;
	}
	
	
	Soul.loaded('events');
})(window);