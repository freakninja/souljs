"use strict";
(function(window){

	var Soul = window.Soul,
		elements = Soul.elements ,
		base = elements.base ;
	
	Soul.Color = function(HTMLColor,Alpha) {
		if(!HTMLColor) HTMLColor = '#81f676';
		if(!Alpha) Alpha = 1;
		HTMLColor = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(HTMLColor.substring(1));
		this.r = parseInt(HTMLColor[1], 16);
		this.g = parseInt(HTMLColor[2], 16);
		this.b = parseInt(HTMLColor[3], 16);
		this.a = Alpha ;
	};
	
	Soul.Color.prototype.toString = function() {
		return 'rgba(' + this.r + ',' + this.g + ',' + this.b + ',' + this.a + ')';
	}
	
	Soul.loaded('color');
})(window);