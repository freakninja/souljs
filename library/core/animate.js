"use strict";
(function(window){

	var Soul = window.Soul,
		elements = Soul.elements ,
		base = elements.base,
		animate, 
		stack,
		engine,
		nextFrame;
	
	stack = [];
	// nextFrame function
	nextFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function(/* function */ callback){ window.setTimeout(callback, 1000 / 60); };
	
	/**
	 * Add fn to the function's animation stack
	 * 
	 * @param fn {Function} 
	 */
	animate = function(fn) {
		for (var s = 0; s < stack.length ; s++ ) {
			if(stack[s].fn == fn) {
				return stack[s];
			}
		}
		var obj = {
			guid  : function() { if ( !this._guid_)  this._guid_= Soul.guid(); return this._guid_; },
			run   : false,
			fn    : fn,
			on    : base.prototype.on,
			start : function() { Soul.dispatchEvent('animationStart',{guid:this.guid()},this); this.run = true; engine.start() },
			stop  : function() { Soul.dispatchEvent('animationStop',{guid:this.guid()} ,this); this.run = false; },
			counters : function() {
				return {
					fps    : engine.counters.fps,
					start  : engine.counters.start,
					frames : engine.counters.frames,
					time   : engine.counters.time,
					ms     : engine.counters.ms
				};
			}
		};
		
		stack.push(obj);
		
		return obj;
	} ;
	
	engine = {
		run : false,
		fps : 33,
		frame : null,
		counters : {
			fps    : 0,
			start  : 0,
			frames : 0,
			time   : 0,
			ms     : 0,
		},
		loop : function(tms)
		{
			if(!this.run)
				return;
			
			var now, ms = 1000 / this.fps, delta, skip;
			
			this.frame = nextFrame.call(window,this.loop.bind(this));
					
			now   = (new Date().getTime());
			delta = now - this.counters.time;
			
			this.counters.ms = tms;
			
			if(delta > ms) {	
				this.counters.fps = Math.ceil( this.counters.frames / ((now - this.counters.start)/1000) );
		
				this.counters.time = now - ( delta % ms );
				
				this.counters.frames++;
				
				skip = 0;
				
				for(var o in stack)
					if ( stack[o].run == true ) {
						stack[o].fn.call(window, tms);
						Soul.dispatchEvent('nextFrame',{ms: tms},stack[o]);
					} else { 
						skip++;
					}
				
				if(skip == stack.length)
					this.stop();
			}
			return;
		},
		start : function()
		{
			if(this.run)
				return;
			this.run = true;
			this.counters.start = (new Date().getTime());
			nextFrame.call(window,engine.loop.bind(this));
		},
		stop : function()
		{
			this.run = false;
		}
		
		
	}
	
	Soul.animate = animate;
	
	Soul.loaded('animate');
})(window);