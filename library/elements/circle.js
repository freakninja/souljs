"use strict";
(function(window){

	var Soul = window.Soul,
		elements = Soul.elements || {},
		circle ;
	
	circle = function(Circle) {

		this._ready();
		
		var self = this;
		
		Circle = Circle || { x : 0 , y : 0 , radius: 10 , color : '#AEAEAE', alpha : 1 };
		
		this.pos = {
			x    : this.prop('x',Circle),
			y    : this.prop('y',Circle) || 0,
		};
		
		this.size = {
			radius: 	this.prop('radius',Circle) || 10 ,
		};
		
		this.x      = this.pos.x;
		this.y      = this.pos.y;
		this.radius = this.size.radius;
		this.color  = new Soul.Color(this.prop('color',Circle), this.prop('alpha',Circle));

		this.rootElement = Circle;
		
		this.position = this.prop('position', Circle) || 'absolute';
		this.visible = this.prop('visible', Circle) || true;
		
		return this;
	};

	circle.prototype = new elements.base ;
	
	circle.prototype.constructor = circle;

	circle.prototype.type = 'soul/x-circle';

	circle.prototype.struct = {
		x : 'int',
		y : 'int',
		visibile: 'bool',
		color: 'string',
		alpha: 'float',
		radius: 'float',
		position: 'string',
		visible: 'bool'
	};
	
	circle.prototype.draw = function(context) {
		
		if( this.visible) { 
			context.save();
			this.prepare(context);
			context.beginPath();
			context.arc(this.x, this.y, this.radius, 0, 2 * Math.PI, false);
			context.fillStyle = this.color.toString();
			context.fill();
			context.closePath();
			context.restore();
		}
		
		for(var child  in this.__children__ ) {
			child = this.__children__[child];
			child.draw(context);
		}
		
		this.dispatchEvent('drawn');
		
	}
	
	elements.circle = circle;
	
	
	Soul.loaded('circle');
	
})(window);