"use strict";
(function(window){

	var Soul = window.Soul,
		elements = Soul.elements || {},
		sprite ;
		
	sprite = function(HTMLImgElement) {
		
		elements.img.call(this,HTMLImgElement);
		
		this.width = this.frameWidth = this.prop('frameWidth',HTMLImgElement);
		this.height = this.frameHeight = this.prop('frameHeight',HTMLImgElement);

		this.frame = { x: 0 , y : 0, row : 0, sprite : 0 }; 
		
		return this;
	};

	sprite.prototype = new elements.img;
	
	sprite.prototype.constructor = sprite;

	sprite.prototype.struct = {
		x : 'int',
		y : 'int',
		width: 'float',
		naturalWidth : 'int',
		frameWidth : 'int',
		height: 'float',
		naturalHeight : 'int',
		frameHeight : 'int',
		src: 'string',
		position: 'string',
		visible: 'bool',
		color: 'string',
		alpha: 'float'
	};
		
	sprite.prototype.type = 'soul/x-sprite';
	
	sprite.prototype.set = function(row, sprite) {
		this.frame.row = row;
		this.frame.sprite = sprite;
	};

	
	sprite.prototype.draw = function(context) {
		var x = this.frame.sprite * this.frameWidth,
		    y = this.frame.row * this.frameHeight;
		this.cut(x,y,this.frameWidth,this.frameHeight);
		elements.img.prototype.draw.call(this, context);
		
		this.dispatchEvent('drawn');
	};
	
	sprite.prototype.cut = function(x,y,w,h) {
		this.size = {
			x     : x || 0,
			y     : y || 0,
			width : w || this.rootElement.naturalWidth,
			height: h || this.rootElement.naturalHeight,
		};
	}
	
	
	elements.sprite = sprite;
	
	Soul.loaded('sprite');
	
})(window);