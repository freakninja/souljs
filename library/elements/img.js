"use strict";
(function(window){

	var Soul = window.Soul,
		elements = Soul.elements || {},
		img;
		
	img = function(HTMLImgElement) {

		var srcElm, self;
		
		if(typeof HTMLImgElement == 'undefined')
			return;
		
		this.contentReady = false;
		
		self = this;
		
		srcElm = document.createElement('img');		
		
		srcElm.onload = srcElm.onreadystatechange = function(){
			if(self.rootElement.complete && (typeof self.rootElement.naturalWidth !== "undefined" && self.rootElement.naturalWidth > 0) ) {
				self.rootElement.width = self.rootElement.width > 0 ? self.rootElement.width : self.rootElement.naturalWidth;
				self.rootElement.height = self.rootElement.height > 0 ? self.rootElement.height : self.rootElement.naturalHeight;
				self.size = {
					x     : 0,
					y     : 0,
					width :	self.rootElement.naturalWidth,
					height: self.rootElement.naturalHeight,
				};
				self._ready();
			}
		};
		
		this.defElement  = HTMLImgElement;
		this.rootElement = srcElm;

		srcElm.src = this.prop('src',HTMLImgElement);
		
		this.pos = {
			x    : this.prop('x',HTMLImgElement),
			y    : this.prop('y',HTMLImgElement),
		};
		
		this.size = {
			x     : 0,
			y     : 0,
			width : this.prop('width',HTMLImgElement),
			height: this.prop('height',HTMLImgElement),
		};
		
		this.x      = this.pos.x;
		this.y      = this.pos.y;
		this.width  = this.size.width;
		this.height = this.size.height;
		
		this.position = this.prop('position', HTMLImgElement) || 'absolute';
		this.visible = this.prop('visible', HTMLImgElement) || true;
		
		return this;
	};

	img.prototype = new elements.base;
	
	img.prototype.constructor = img;

	img.prototype.struct = {
		x : 'int',
		y : 'int',
		width: 'float',
		naturalWidth : 'int',
		height: 'float',
		naturalHeight : 'int',
		src: 'string',
		position: 'string',
		visible: 'bool',
		color: 'string',
		alpha: 'float'
	};
		
	img.prototype.type = 'soul/x-img';
	
	img.prototype.draw = function(context) {
	
		context.save();
		
		this.prepare(context);
		
		var d = this.getDimensions();
		var x = d.x + d.transform.x , 
			y = d.y + d.transform.y;
		
		if( this.visible) {
			context.drawImage(this.rootElement, // image source
				this.size.x, this.size.y, this.size.width, this.size.height, // srcX, srcY, srcW, srcH
				x, y ,this.width, this.height); // dstX, dstY, dstW, dstH
		
		}
		
		context.restore();
		
		for(var child  in this.__children__ ) {
			child = this.__children__[child];
			child.draw(context);
		}
		
		this.dispatchEvent('drawn');
	}
		
	
	img.prototype.reset = function() {
		this.size = {
				x     : 0,
				y     : 0,
				width : this.rootElement.naturalWidth,
				height: this.rootElement.naturalHeight,
		};
		this.x      = this.pos.x;
		this.y      = this.pos.y;
		this.width  = this.prop('width',this.defElement) || this.size.width;
		this.height = this.prop('height',this.defElement) || this.size.height;
		this.position = this.prop('position', this.defElement) || 'absolute';
		this.visible = this.prop('visible', this.defElement) || true;
	}
	
	img.prototype.clip = function(x,y,w,h) {
		this.size = {
			x     : x || 0,
			y     : y || 0,
			width : w / (this.width / this.rootElement.naturalWidth) || this.rootElement.naturalWidth,
			height: h / (this.height / this.rootElement.naturalHeight) || this.rootElement.naturalHeight,
		};
	}
	
	elements.img = img;
	
	Soul.loaded('img');
	
})(window);