"use strict";
(function(window){

	var Soul = window.Soul,
		elements = Soul.elements || {},
		canvas;

	canvas = function(HTMLCanvasElement) {
		
		this._ready();
		
		if (!HTMLCanvasElement || !HTMLCanvasElement.tagName || !(/canvas/i).test(HTMLCanvasElement.tagName) || !HTMLCanvasElement.getContext || !HTMLCanvasElement.getContext('2d') ) {
			HTMLCanvasElement = document.createElement('canvas');
			HTMLCanvasElement.width = 800;
			HTMLCanvasElement.height = 600;
		}
		
		this.rootElement = HTMLCanvasElement;
		
		this.context = HTMLCanvasElement.getContext('2d');
		
		this.size = {
			width: 	HTMLCanvasElement.width,
			height: HTMLCanvasElement.height,
		};
		
		this.width = this.size.width;
		this.height = this.size.height;

		return this;
	};

	canvas.prototype = new elements.base;
	
	canvas.prototype.constructor = canvas;

	canvas.prototype.type = 'soul/x-canvas';
	
	/**
	 * Clear the canvas
	 * @param w
	 * @param h
	 * @returns {canvas}
	 */
	canvas.prototype.clear = function(w,h) {
		w = w || this.width;
		h = h || this.height;
		this.context.save();
		this.context.translate(0,0);
		this.context.clearRect(0,0,w, h);
		this.context.restore();
		return this;
	};
	
	/**
	 * Draw all elements on canvas
	 * 
	 * @returns {canvas}
	 */
	canvas.prototype.draw = function() {
		for(var child  in this.__children__ ) {
			//this.context.save();
			this.__children__[child].draw(this.context);
			//this.context.restore();
		}
		
		this.dispatchEvent('drawn');
		
		return this;
	}
	/**
	 * redraw sames as clear && draw
	 * 
	 * @returns {canvas}
	 */
	canvas.prototype.redraw = function() {
		this.clear();
		this.draw();
		return this;
	}
	
	elements.canvas = canvas;
	
	Soul.loaded('canvas');
	
})(window);