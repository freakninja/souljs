"use strict";
(function(window){

	var Soul = window.Soul,
		elements = Soul.elements || {},
		rect ;
	
	rect = function(Rect) 
	{
		this._ready();
		
		var self = this;
		
		Rect = Rect || { x : 0 , y : 0 , width : 100 , height: 100 , color : '#AEAEAE', alpha : 1 };
		
		this.pos = {
			x    : this.prop('x', Rect),
			y    : this.prop('y', Rect),
		};
		
		this.size = {
			width: 	this.prop('width', Rect) || 100 ,
			height: this.prop('height', Rect) || 100,
		};
		
		this.x      = this.pos.x;
		this.y      = this.pos.y;
		this.width  = this.size.width;
		this.height = this.size.height;
		
		this.color  = new Soul.Color(this.prop('color', Rect), this.prop('alpha', Rect));
		
		this.position = this.prop('position', Rect) || 'absolute';
		this.visible = this.prop('visible', Rect) || true;
		
		return this;
	};

	rect.prototype = new elements.base;
	
	rect.prototype.constructor = rect;

	rect.prototype.type = 'soul/x-rect';
	
	rect.prototype.draw = function(context) {
		
		context.save();
		
		this.prepare(context);
		
		var d = this.getDimensions();
		var x = d.x + ( (d.transform && d.transform.x) ? d.transform.x : 0 ), 
			y = d.y + ( (d.transform && d.transform.y) ? d.transform.y : 0 );
	
		if( this.visible) {
			context.fillStyle = this.color.toString();
			context.fillRect(x, y, d.width, d.height);
		}
		
		context.restore();
		
		for(var child  in this.__children__ ) {
			child = this.__children__[child];
			child.draw(context);
		}
		
		this.dispatchEvent('drawn');
		
		return this;
	};
	
	rect.prototype.reset = function() {
		this.x      = this.pos.x;
		this.y      = this.pos.y;
		this.width  = this.size.width;
		this.height = this.size.height;
	};
	
	elements.rect = rect;
	
	Soul.loaded('rect');
	
})(window);