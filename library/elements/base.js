"use strict";
(function(window){

	var Soul = window.Soul,
		base;

	base = function() {};
	
	/**
	 * If true the object is visible on the canvas
	 * 
	 * @type {Boolean} 
	 */
	base.prototype.visible = true;
	
	/**
	 * Define the positioning style smiliar to CSS
	 * 
	 * @type {Enum} [ absolute, relative ]
	 */
	base.prototype.position = 'absolute';
	
	/**
	 * Root element
	 * 
	 * @type {HTMLElement}
	 */
	base.prototype.rootElement = null;
	
	/**
	 * Object struct
	 */
	base.prototype.struct = {
		x : 'int',
		y : 'int',
		width: 'float',
		height: 'float',
		position: 'string',
		visible: 'bool',
		color: 'string',
		alpha: 'float',
		tickness: 'float'
	};
	
	/**
	 * Object type identifier
	 * 
	 * @type {String}
	 */
	base.prototype.type = 'soul/x-base';

	/**
	 * GUID/UUID rfc4122 version 4 compliant
	 * 
	 * @return {String}
	 */
	base.prototype.guid = function()
	{
		if ( typeof this.__guid__ == 'undefined' ) 
			this.__guid__ = Soul.guid();
			
		return this.__guid__;
	};
	
	/**
	 * Sets/Returns the object id
	 * 
	 * @param id 
	 * @returns {String}
	 */
	base.prototype.id = function(id)
	{
		if ( typeof this.__id__ == 'undefined' ) 
			this.__id__ = id;
		if ( typeof id != 'undefined' ) {
			this.__id__ = id;
			return this;
		} 
		
		return this.__id__;
		
	};
	
	/**
	 * Add a class to the object
	 * 
	 * @param name
	 * @returns {base}
	 */
	base.prototype.addClass = function(name) {
		if ( typeof name == 'undefined' ) 
			return this;
		
		if ( typeof this.__classes__ == 'undefined' ) { 
			this.__classes__ = [name];
		} else if(this.__classes__.indexOf(name) == -1) {
			this.__classes__.push(name);
		}
		
		return this;
	};
	
	/**
	 * Remove a class from the object
	 * 
	 * @param name
	 * @returns {base}
	 */
	base.prototype.removeClass = function(name) {
		
		if ( typeof name == 'undefined' ) 
			return this;
		var i;
		
		if ( ( i = this.__classes__.indexOf(name)) == -1) {
			return;
		} 
		
		this.__classes__.splice(i,1);
		
		return this;
	};
	
	/**
	 * TRUE if class is present, FALSE otherwise
	 * @param name
	 * @returns {Boolean}
	 */
	base.prototype.hasClass = function(name) {
		return (typeof this.__classes__ != 'undefined' && this.__classes__.indexOf(name) != -1 );
	};
	
	/**
	 * Append elm to the object children
	 * 
	 * @param elm {soul/x-*}
	 * @returns {base}
	 */
	base.prototype.append = function(elm) {
		elm = Soul(elm);
		
		if( typeof this.__children__ == 'undefined') {
			this.__children__ = [];
		}
		elem.__parent__ = this;
		this.__children__.push(elm);
		if(elm.contentReady)
			this._ready(true);
		return this;
	};
	
	/**
	 * Append this object to elm
	 * 
	 * @param elm {soul/x-*}
	 * @returns {base}
	 */
	base.prototype.appendTo = function(elm) {
		elm = Soul(elm);
		
		if( typeof elm.__children__ == 'undefined') {
			elm.__children__ = [];
		}
		this.__parent__ = elm;
		elm.__children__.push(this);
		
		if(this.contentReady)
			elm._ready(true);
		
		return this;
	};
	
	/**
	 * Removes elm from this object children
	 * 
	 * @param elm {soul/x-*}
	 * @returns {base}
	 */
	base.prototype.remove = function(elm) {
		elm = Soul(elm);
		var i;
		if( typeof this.__children__ == 'undefined') {
			this.__children__ = [];
			return;
		}
		if ( (i = this.__children__.indexOf(elm)) == -1 ) {
			return;
		}
		elm.__parent__ = this;
		this.__children__.splice(i,1);
		return this;
	};
	
	/**
	 * Remove this object from elm children
	 * 
	 * @param elm
	 * @returns {base}
	 */
	base.prototype.removeFrom = function(elm) {
		elm = Soul(elm);
		var i;
		if( typeof elm.__children__ == 'undefined') {
			elm.__children__ = [];
			return;
		}
		if ( (i = elm.__children__.indexOf(this)) == -1 ) {
			return;
		}
		this.__parent__ = elm;
		elm.__children__.splice(i,1);
		return this;
	};
	/**
	 * Get current object parent
	 * 
	 * @returns {soul/x-*}
	 */
	base.prototype.parent = function() {
		return this.__parent__;
	};
	/**
	 * Get current object children
	 * 
	 * @returns {Array:soul/x-*}
	 */
	base.prototype.children = function() {
		return this.__children__;
	};

	base.prototype._ready = function(child)
	{
		this.contentReady = true;
		return this;
	};
	
	base.prototype.ready = function(fn)
	{
		if(this.isReady()) {
			fn.call(this);
			return;
		}
		var self = this; 
		setTimeout(function(){
			self.ready(fn);
		},9);
	};
	
	/**
	 * TRUE if this object and all its children are ready for drawing, FALSE otherwise
	 * 
	 * @returns {Boolen}
	 */
	base.prototype.isReady = function() {
		var ret = this.contentReady;
		for(var child in this.__children__ ) {
			child = this.__children__[child];
			if ( typeof child.isReady == 'function' ) {
				ret = ret & child.isReady();
			} 
		}
		return ret;
	}
	
	/**
	 * Prepare the object for drawing
	 * 
	 * @param context
	 */
	base.prototype.prepare = function(context) {
		if(!this.prep) return;
		
		for(var prop in this.prep) {
			if(typeof context[prop] == 'function') {
				var args = this.prep[prop];
				if (typeof args.push != 'function' || typeof args.pop != 'function' )
					args = [args];
				context[prop].apply(context,args)
			} else if (typeof context[prop] != 'undefined') {
				context[prop] = this.prep[prop];
			}
		}
	};
	
	/**
	 * Returns object dimentions
	 * 
	 * @returns {x,y,width,height,radius,transform}
	 */
	base.prototype.getDimensions = function()
	{
		var out = {};
		out.x = this.x || 0; 
		out.y = this.y || 0;
		out.width = this.width || 0;
		out.height = this.height || 0;
		out.radius = this.radius || 0;
		out.tickness = this.tickness || 0;
		out.transform = this.transform || { x : 0 , y : 0 };
		if (this.position == 'relative') {
			var parent = this.parent().getDimensions();
			out.x = parent.x + out.x || 0;
			out.y = parent.y + out.y || 0;
		}
		
		return out;
	};
	
	/**
	 * Returns 'name' property from object 'object'
	 * 
	 * @param name
	 * @param object
	 * @returns {mixed}
	 */
	base.prototype.prop = function(name, object) 
	{
		var cast, 
			value;
		
		if ( typeof object == 'undefined' ) object = this.rootElement;
		
		if(!(cast = this.struct[name])) {
			throw Soul.Exception(this,this,"Invalid property '"+name+"' for object '"+this.type+"'"); 
		}

		if(object.getAttribute && object.getAttribute(name)) {
			value = object.getAttribute(name);
		} else /* if ( typeof object[name] != 'undefined' )*/ {
			value = object[name];
		}
		
		switch(cast) {
		case 'int':
			value = parseInt(value) || 0;
			break;
		case 'float':
			value = parseFloat(value) || 0;
			break;
		case 'string':
			value = value || '';
			break;
		case 'bool':
			value = value === true ;
			break;
		}
		
		return value;
	};
	
	/**
	 * Draw on the context
	 * 
	 * @param context
	 */
	base.prototype.draw = function(context) {
		this.prepare(context);
	};
	/**
	 * Reset object to its initial state
	 */
	base.prototype.reset = function() { };
	
	Soul.elements = Soul.elements || {};
	
	Soul.elements.base = base;
	// base is loaded.
	Soul.loaded('base');
})(window);