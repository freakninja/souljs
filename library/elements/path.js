"use strict";
(function(window){

	var Soul = window.Soul,
		elements = Soul.elements || {},
		path ;
	
	path = function(Path) {

		this._ready();
		
		var self = this;
		
		Path = Path || { x : 0 , y : 0 , tickness: 3 , color : '#AEAEAE', alpha : 1 };
		
		this.pos = {
			x : this.prop('x', Path),
			y : this.prop('y', Path),
		};
		
		this.size = {
			width: 	this.prop('tickness', Path) || 3 ,
		};
		
		this.x      = this.pos.x;
		this.y      = this.pos.y;
		this.tickness  = this.size.tickness;
		this.color  = new Soul.Color(this.prop('color', Path), this.prop('alpha', Path));
		
		this.rootElement = Path;
		
		this.position = this.prop('position', Path) || 'absolute';
		this.visible = this.prop('visible', Path) || true;
		
		this.__points__ = [];
		
		return this;
	};

	path.prototype = new elements.base;
	
	path.prototype.constructor = path;

	path.prototype.type = 'soul/x-path';
	
	path.prototype.draw = function(context) {

		if( !this.visible)
			return ;
		
		context.save();
		
		this.prepare(context);
		
		context.beginPath();
		
		var d = this.getDimensions();
		
		context.moveTo(d.x + d.transform.x, d.y + d.transform.y);
		
		for(var p  in this.__points__ ) {
			p = this.__points__[p];
			context.lineTo(p.x + d.x + d.transform.x,p.y + d.y + d.transform.y);
		}
		
		if( this.visible) { 
			context.lineWidth   = this.tickness;
			context.strokeStyle = this.color.toString();
	    	context.stroke();
		}
		
		context.closePath();
		
		context.restore();
		
		this.dispatchEvent('drawn');
		
		return this;
	};
	
	path.prototype.addPoint = function(x,y)
	{
		this.__points__.push({x : x ,y : y});
		this._updateSize_();
	};
	
	path.prototype.delPoint = function(index)
	{
		if(this.__points__[index])
			this.__points__ = this.__points__.splice(index, 1)
		this._updateSize_();
	};
	
	path.prototype.points = function()
	{
		return this.__points__;
	};
	
	path.prototype._updateSize_ = function()
	{
		var mX = 0, MX = 0, mY = 0, MY = 0;
		for(var p in this.__points__) {
			var p = this.__points__[p];
			mX = Math.min(mX,p.x);
			MX = Math.max(MX,p.x);
			mY = Math.min(mY,p.y);
			MY = Math.max(MY,p.y);
		} 
		
		this.width = Math.abs(mX) + Math.abs(MX) ;
		this.height = Math.abs(mX) + Math.abs(MX) ;
	};
	
	
	elements.path = path;
	
	
	Soul.loaded('path');
	
})(window);