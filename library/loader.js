"use strict";
var Soul = Soul || {};
Soul.modules = {
	//        file                , loaded flag, timeout, script guid
	engine : ['core/engine.js'    , false      , 250    , null, []],
	color  : ['core/color.js'     , false      , 250    , null, ['engine']],
	animate: ['core/animate.js'   , false      , 250    , null, ['engine','base','events']],
	events : ['core/events.js'    , false      , 250    , null, ['engine','base']],
	base   : ['elements/base.js'  , false      , 250    , null, ['engine']],
	effects: ['core/effects.js'   , false      , 250    , null, ['engine','base']],
	canvas : ['elements/canvas.js', false      , 250    , null, ['engine','base','events']],
	img    : ['elements/img.js'   , false      , 250    , null, ['engine','base']],
	sprite : ['elements/sprite.js', false      , 250    , null, ['engine','base','img']],
	rect   : ['elements/rect.js'  , false      , 250    , null, ['engine','base','color']],
	circle : ['elements/circle.js', false      , 250    , null, ['engine','base','color']],
	path   : ['elements/path.js'  , false      , 250    , null, ['engine','base','color']],
};
/**
 * GUID/UUID rfc4122 version 4 compliant
 * 
 * @return string
 */
Soul.guid = function() {
	return 'xxxxxxxx-yxxx-yxxx-yxxx-yxxxxxxxxxxx'.replace(/[xy]/g, function(c) {var r = Math.random()*16|0,v=c=='x'?r:r&0x3|0x8;return v.toString(16);});
};

Soul.broken = false;

Soul.rootElement = document.getElementById('SoulJS');

Soul.baseURL = Soul.rootElement.getAttribute('data-main');

Soul.require = function(module) {
	
	if(typeof Soul.modules[module] == 'undefined')
		return;

	var modules = Soul.modules ,
	    definition = modules[module],
	    dependencies = modules[module][4],
	    dependency ;
	
	if( definition[3] != null && definition[3] != 'pending' ) 
		return; // already requested, already loaded.
	
	modules[module][3] = 'pending'; // pending module status.
	
	for(var i = 0 ; i < dependencies.length; i++) {
		
		dependency = modules[dependencies[i]];
		
		if( dependency[1] == true )
			continue;
		
		if(dependency[1] == false && dependency[3] == null )
			Soul.require(dependencies[i]);
	
		return setTimeout(function(){
			Soul.require(module);
		},9)
	}
	
	var s = document.createElement('script');
	Soul.modules[module][3] = s.id = Soul.guid();
	s.setAttribute('data-module', module);
	s.setAttribute('data-guid', Soul.modules[module][3]);
	s.src = Soul.baseURL + Soul.modules[module][0] + '?' + (new Date()).getTime();
	
	(document.getElementsByTagName('head')[0]).appendChild(s);
	
	Soul.log('required',module, Date.now());
	
	setTimeout(function(){
		if ( Soul.modules[module][1] == false ) {
			throw Soul.Exception(Soul.modules[module],this,"Unable to load module '" + module + "' from '"+Soul.baseURL + Soul.modules[module][0]+"'");
		}
	},Soul.modules[module][2]);
	
};

Soul.log = function() {
	if(Soul.debug) 
		console.log(arguments);
};

Soul.depend = function(m, d) {
	Soul.modules[m][4] = d;
};


Soul.loaded = function(module) {
	if ( Soul.modules && Soul.modules[module]) {
		Soul.log('loaded',module, Date.now());
		Soul.modules[module][1] = true;
	};
};

Soul.render = function(fn) {
	if ( ! Soul.__ready__ ) Soul.__ready__ = [fn];
	else if ( fn ) Soul.__ready__.push(fn);
	
	var mod, module;
	for(mod in Soul.modules) {
		module = Soul.modules[mod];
		if(module[3]!= null && module[1] == false) {
			return setTimeout(function(){Soul.render();}, 9);
		}
	}
	for(var fn in Soul.__ready__ ) Soul.__ready__[fn].call(window);
	Soul.__ready__ = [];
};

Soul.Exception = function(element, context, message)
{
	return { Exception : 
		{ 
			message : message ,
			object : element,
			objectType :  element.type ? element.type : 'unknown', 
			context : context
		} 
	};
}

Soul.require('engine');